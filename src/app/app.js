﻿'use strict';

class Shape {

    constructor (x, y) {
        this.x = x;
        this.y = y;
    }

    myFunction () {
        return this.x * this.y;
    }

    myFunction2 (evens) {
        return evens.map(v => v + 1);
    }
}
