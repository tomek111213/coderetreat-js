module.exports = function (config) {
    config.set({
        basePath: 'src',
        frameworks: ['jasmine'],
        files: [
            'app/**/*.js',
            'tests/**/*.js'
        ],
        reporters: ['progress', 'html', 'coverage'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['PhantomJS'],
        captureTimeout: 60000,
        singleRun: true,
        preprocessors: {
            'app/**/*.js': ['babel', 'coverage'],
            'tests/**/*.js': ['babel']
        },
        htmlReporter: {
            outputFile: '../build/reports/test_report.html'
        },
        coverageReporter: {
            type: 'html',
            dir: '../build/reports/coverage/',
            file: 'coverage.xml'
        },
        babelPreprocessor: {
            options: {
                presets: ['es2015'],
                sourceMap: 'inline'
            }
        }
    });
};
