### JavaScript Template ###

#### How to use it ####

Call `npm install` to install dependencies. Now you can call `grunt` to build the project or `grunt test` to run tests after each change (works better without auto-save).
    
#### How to quickly create setup like this ####

I assume that you have NodeJS installed.

    npm init -y
    npm install --save-dev babel-preset-env
    npm install --save-dev babel-preset-es2015
    npm install --save-dev grunt
    npm install --save-dev grunt-babel
    npm install --save-dev grunt-cli
    npm install --save-dev grunt-contrib-clean
    npm install --save-dev grunt-contrib-jasmine
    npm install --save-dev grunt-contrib-watch
    npm install --save-dev grunt-karma
    npm install --save-dev jasmine-core
    npm install --save-dev karma
    npm install --save-dev karma-babel-preprocessor
    npm install --save-dev karma-coverage
    npm install --save-dev karma-htmlfile-reporter
    npm install --save-dev karma-jasmine
    npm install --save-dev karma-phantomjs-launcher
    npm install --save-dev load-grunt-tasks
    npm install --save-dev protractor
    
You can fill missing details `package.json` if you want. 

    touch .gitignore
    touch .babelrc
    touch Grunfile.js
    touch karma.conf.js

Fill `.gigignore` with:

    node_modules
    .idea/
    build/

Fill `.babelrc` with:

    {
      "presets": ["es2015"]
    }


Fill `Grunfile.js`, `karma.conf.js` using documentation or files from this project.
